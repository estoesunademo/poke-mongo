from pymongo import *
from pprint import pprint
from mongoengine import *

import os

from Pokemon import *
from dotenv import load_dotenv

load_dotenv()

mongodb_uri = os.getenv('MONGODB_URI')
client = connect(host=mongodb_uri)

tina = Pokemon(
    id_field = 81,
    num="081",
    name="Tina",
    img="tina.png",
    type=[Type.NORMAL],
    height="1.5m",
    weight="50kg",
    candy="Normal Candy",
    candy_count=25,
    egg="5km",
    spawn_chance=0.01,
    avg_spawns=10,
    spawn_time="Day",
    multipliers=[1.5],
    weaknesses=[Type.FIGHT],
    next_evolution=[
        Evolution(num="002", name="TinotaToon"),
        Evolution(num="003", name="ChainochaChun")
    ]
)

tina.save()
pprint(tina)