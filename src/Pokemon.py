from enum import Enum

from mongoengine import *


class Type(Enum):
    BUG = "Bug"
    DARK = "Dark"
    DRAGON = "Dragon"
    ELECTRIC = "Electric"
    FAIRY = "Fairy"
    FIGHT = "Fight"
    FLYING = "Flying"
    GHOST = "Ghost"
    GROUND = "Ground"
    NORMAL = "Normal"
    ROCK = "Rock"
    STEEL = "Steel"
    POISON = "Poison"
    GRASS = "Grass"
    FIRE = "Fire"
    ICE = "Ice"
    PSYCHIC = "Psychic"
    WATER = "Water"


class Evolution(EmbeddedDocument):
    num = StringField()
    name = StringField()


class Pokemon(Document):
    # id = IntField()
    id_field = IntField(required=True, db_field = "id") #para que no entre en conflicto con el ID autogenerado
    num = StringField()
    name = StringField()
    img = StringField()
    type = ListField(EnumField(Type))
    height = StringField()
    weight = StringField()
    candy = StringField()
    candy_count = IntField()
    egg = StringField()
    spawn_chance = FloatField()
    avg_spawns = IntField()
    spawn_time = StringField()
    multipliers = ListField(FloatField())
    weaknesses = ListField(EnumField(Type))
    next_evolution = ListField(EmbeddedDocumentField(Evolution))
    prev_evolution = ListField(EmbeddedDocumentField(Evolution))

    def __str__(self):
        return f"Pokemon: {self.name} ({self.num})"

class Move(Document):
    name = StringField()
    pwr = FloatField(min_value=0, max_value=180)
    type = EnumField(Type)

    meta = {'allow_inheritance': True}


class FastMove(Move):
    movetype = StringField(default="Fast")
    energyGain = FloatField(min_value=0, max_value=20)


class ChargedMove(Move):
    movetype = "Charged"  # hace lo mismo que default...
    energyCost = FloatField(min_value=33, max_value=100)


class Team(Document):
    num = StringField()
    name = StringField()
    type = ListField(EnumField(Type))
    catch_date = DateTimeField()
    CP = FloatField()
    HPmax = FloatField(min_value=200, max_value=1000)
    HP = FloatField()
    atk = IntField(min_value=10, max_value=50)
    def_field = IntField(field_name="def", min_value=10, max_value=50)  # def está reservada, hay que hacer esta movida
    energy = IntField(min_value=0, max_value=100)
    moves = ListField(ReferenceField(Move))
    candy = StringField()
    candy_count = FloatField()
    current_candy = FloatField()
    weaknesses = ListField(EnumField(Type))