def search(collection, name, attribute):
    try:
        search_pokemon = (
            {"id": int(name)}
            if name.isdigit()
            else {"name": name.lower().capitalize()}
        )
        pokemon = collection.find_one(search_pokemon)
        if not pokemon:
            print(f"No se encontró ningún resultado con la siguiente búsqueda {search_pokemon}")
            return

        try:
            if attribute == "evol":
                arrayEv = []
                ########## TE LO RESUMO NO MÁS ###########
                if "prev_evolution" in pokemon:
                    for prev in pokemon["prev_evolution"]:
                        arrayEv.append(prev["name"])
                arrayEv.append(pokemon["name"])
                if "next_evolution" in pokemon:
                    for next in pokemon["next_evolution"]:
                        arrayEv.append(next["name"])
                ##########################################
                print(arrayEv)
            else:
                tutupla = (pokemon["name"], pokemon[attribute])
                print(tutupla)
        except:
            print(f"ERROR: value {attribute} no existe...")

    except Exception as e:
        print("ERROR: " + str(e))
